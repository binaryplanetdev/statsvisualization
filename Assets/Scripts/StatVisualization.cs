﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class StatVisualization : MonoBehaviour {
    public static string PLAYER_IMAGE_PATH = "file://";
    public static string TEAM_IMAGE_PATH = "file://";
    public static string TEXT_PATH = "file://";

    public GameObject m_goMainCamera;
    public GameObject m_goStatsChartContainer;
    public GameObject m_goBarChartContainer;
    public GameObject m_goLineChartContainer;
    public GameObject m_goPieChartContainer;
	public GameObject m_goVisualStatsContainer;
    public GameObject m_goTitle;

    public GameObject m_EffectAnimation;

    //public GameObject m_goLightAnimation;
    public GameObject m_goUIContainer;
    
    private GameObject m_goPlayer;

    public GameObject m_goAText;
    
	private string[] m_sChartType = new string[] { "bar", "line", "pie", "stats", "futures_defence", "futures_offence", "advance_base", "steal", "pinch_hitter", "ball_type_left", "ball_type_right" };

    private int m_nLightImageIndex = 0;
    private int m_nLightImageMaxIndex = 22;
    private float m_nLightAnimStartDelay = 1.5f;
    private float m_nLightAnimDelay = 0.05f;

    private Vector3 m_vMainCameraPosition;
        
    void Awake() {
        PLAYER_IMAGE_PATH = "file://";
        TEAM_IMAGE_PATH = "file://";
        TEXT_PATH = "file://";

        m_vMainCameraPosition = new Vector3(50f, 32f, -80f);

        this.m_goMainCamera.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
        this.m_goMainCamera.transform.localPosition = this.m_vMainCameraPosition;

    }
    
    public void startStatVisualization(string p_sJSONData)
    {
        try
        {
            try
            {
                Image lightAnimationImage = this.m_EffectAnimation.transform.GetComponentInChildren<Image>();
                lightAnimationImage.sprite = Resources.Load<Sprite>("Images/Sprites/StarEffects/effect_00000");
                this.m_EffectAnimation.SetActive(false);
            }
            catch { }

            //Debug.Log("startStatVisualization : Message received - " + p_sJSONData);
            StatsPlayInfo parsedData = NJson.Decode<StatsPlayInfo>(p_sJSONData);

            //if (m_goAText != null)
            //    m_goAText.SetActive(DateTime.Now < new DateTime(2016, 04, 11, 0, 0, 0));

            StartCoroutine("loadText", "c:/message.txt");

            this.drawChart(parsedData);
        }
        catch
        {
            Debug.Log("startStatVisualization : Parsing Error!");
        }
    }

    public void stopStatVisualization()
    {
        this.hideAllGameObject();
    }

    IEnumerator loadText(string p_sFileName)
    {
        WWW www = new WWW(StatVisualization.TEXT_PATH + p_sFileName);

        yield return www;

        try
        {
            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.Log("ERROR : " + www.error);
            }
            else if (www.size > 0)
            {
                try
                {
                    if (www.text.Length > 0)
                    {
                        m_goAText.SetActive(true);

                        Text t = m_goAText.transform.Find("Canvas").Find("AddtionalText").GetComponentInChildren<Text>();
                        t.text = www.text;
                    }
                }
                catch (Exception ex) { Debug.Log(ex.Message); }
                
            }
            else
            {
                m_goAText.SetActive(false);
            }
        }
        catch
        {
            Debug.Log("ERROR while fetching text!");
        }
    }

    public void drawChart(StatsPlayInfo p_parsedData)
    {
        try
        {
			Debug.Log("p_parsedData.ctype : " + p_parsedData.ctype);
            if (p_parsedData == null)
            {
                return;
            }

            if (Array.IndexOf(this.m_sChartType, p_parsedData.ctype) < 0)
            {
                return;
            }

            this.stopStatVisualization();

            //        StartCoroutine("playLightAnimation");

            this.drawTitle(p_parsedData);

            if (p_parsedData.ctype == "bar")
            {
                m_vMainCameraPosition = new Vector3(44f, 40f, -80f);

//                this.m_goMainCamera.transform.localEulerAngles = new Vector3(5f, 3f, 0f);
                this.m_goMainCamera.transform.localEulerAngles = new Vector3(6f, 3f, 0f);
                this.m_goMainCamera.transform.localPosition = this.m_vMainCameraPosition;

                this.drawBarChart(p_parsedData);

                StartCoroutine("playLightAnimation");
            }
            else if (p_parsedData.ctype == "line")
            {
                m_vMainCameraPosition = new Vector3(44f, 40f, -80f);

                this.m_goMainCamera.transform.localEulerAngles = new Vector3(6f, 3f, 0f);
                this.m_goMainCamera.transform.localPosition = this.m_vMainCameraPosition;

                this.drawLineChart(p_parsedData);
            }
            else if (p_parsedData.ctype == "pie")
            {
                m_vMainCameraPosition = new Vector3(44f, 40f, -80f);

                this.m_goMainCamera.transform.localEulerAngles = new Vector3(6f, 3f, 0f);
                this.m_goMainCamera.transform.localPosition = this.m_vMainCameraPosition;

                this.drawPieChart(p_parsedData);
            }
            else if (p_parsedData.ctype == "stats")
            {
                this.drawStatsChart(p_parsedData);
			}
			else if (p_parsedData.ctype == "futures_defence" || p_parsedData.ctype == "futures_offence" || p_parsedData.ctype == "pinch_hitter" 
				|| p_parsedData.ctype == "steal" || p_parsedData.ctype == "advance_base" || p_parsedData.ctype == "ball_type_left" || p_parsedData.ctype == "ball_type_right")
			{
				this.drawVisualStats(p_parsedData);
			}
        }
        catch (Exception ex) { Debug.Log(ex.Message); }
    }

    IEnumerator playLightAnimation()
    {
        yield return new WaitForSeconds(this.m_nLightAnimStartDelay);

        this.m_nLightImageIndex = 0;

        this.m_EffectAnimation.SetActive(true);

        StartCoroutine("updateLightImage");
    }

    IEnumerator updateLightImage()
    {
        Image lightAnimationImage = this.m_EffectAnimation.transform.GetComponentInChildren<Image>();

        try
        {
            lightAnimationImage.sprite = Resources.Load<Sprite>("Images/Sprites/StarEffects/effect_" + this.m_nLightImageIndex.ToString("D5"));
            lightAnimationImage.rectTransform.localScale = new Vector3(1f, 1f, 1f);

            this.m_nLightImageIndex++;
        }
        catch (Exception ex) { Debug.Log(ex.Message); m_nLightImageIndex = 0; }
        yield return new WaitForSeconds(this.m_nLightAnimDelay);

        if (this.m_nLightImageIndex < this.m_nLightImageMaxIndex)
        {
            StartCoroutine("updateLightImage");
        }
        else
        {
            //this.m_nLightImageIndex = 0;
            //StartCoroutine("updateLightImage");
            //this.m_EffectAnimation.SetActive(false);
        }

    }

    void hideAllGameObject() {
        try
        {
            if (this.m_goPlayer != null)
            {
                Player player = this.m_goPlayer.GetComponent<Player>();
                player.hidePlayer();
            }
        }
        catch { }
        finally
        {
            this.m_goPlayer = null;
        }
        
        if (this.m_goTitle != null) {
            this.m_goTitle.SetActive(false);
        }

        try
        {
            this.m_EffectAnimation.SetActive(false);
        }
        catch {}


        //if (this.m_goLightAnimation != null) {
        //    this.m_goLightAnimation.SetActive(false);
        //    m_nLightImageIndex = 1000;
        //}

        this.hideAllChart();
    }

    void hideAllChart() {
        if (this.m_goBarChartContainer != null) {
            BarChartManager barChartManager = this.m_goBarChartContainer.GetComponent<BarChartManager>();
            barChartManager.removeChart();

            this.m_goBarChartContainer.SetActive(false);
        }

        if (this.m_goLineChartContainer != null) {
            LineChartManager lineChartManager = this.m_goLineChartContainer.GetComponent<LineChartManager>();
            lineChartManager.removeChart();

            this.m_goLineChartContainer.SetActive(false);
        }

        if (this.m_goPieChartContainer != null) {
            PieChartManager pieChartManager = this.m_goPieChartContainer.GetComponent<PieChartManager>();
            pieChartManager.removeChart();

            this.m_goPieChartContainer.SetActive(false);
        }

        if (this.m_goStatsChartContainer != null)
        {
            StatsChartManager statsChartManager = this.m_goStatsChartContainer.GetComponent<StatsChartManager>();
            statsChartManager.removeChart();

            this.m_goStatsChartContainer.SetActive(false);
        }

		if (this.m_goVisualStatsContainer != null)
		{
			VisualStatsManager visualSstatsManager = this.m_goVisualStatsContainer.GetComponent<VisualStatsManager>();
			visualSstatsManager.removeChart();

			this.m_goVisualStatsContainer.SetActive(false);
		}

        this.m_goMainCamera.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
        this.m_goMainCamera.transform.localPosition = this.m_vMainCameraPosition;
    }
    
#region Draw 게임 오브젝트
    void drawTitle(StatsPlayInfo p_statsPlayInfo)
    {
        try
        {
            Transform tPlayerNo = this.m_goTitle.transform.FindChild("PlayerBackNo");
            Transform tPlayerName = this.m_goTitle.transform.FindChild("PlayerName");
            Transform tPlayerPosition = this.m_goTitle.transform.FindChild("PlayerPosition");

            tPlayerNo.GetComponent<Text>().text = p_statsPlayInfo.pl_no;
            tPlayerName.GetComponent<Text>().text = p_statsPlayInfo.pl_nm;
            tPlayerPosition.GetComponent<Text>().text = p_statsPlayInfo.pl_pos;

            this.m_goTitle.SetActive(true);

            float fMarginLeft = 0f;

            RectTransform rPlayerNoRectTransform = tPlayerNo.GetComponent<RectTransform>();
            fMarginLeft += this.setTextPosition(rPlayerNoRectTransform, fMarginLeft);
            fMarginLeft += 3f;

            RectTransform rPlayerNameRectTransform = tPlayerName.GetComponent<RectTransform>();
            fMarginLeft += this.setTextPosition(rPlayerNameRectTransform, fMarginLeft);
            fMarginLeft += 3f;

            RectTransform rPlayerPositionRectTransform = tPlayerPosition.GetComponent<RectTransform>();
            fMarginLeft += this.setTextPosition(rPlayerPositionRectTransform, fMarginLeft);

            this.drawPlayerImage(p_statsPlayInfo.pl_img_path);
        }
        catch (Exception ex) { Debug.Log(ex.Message); }
    }

    float setTextPosition(RectTransform p_rTextRectTransform, float p_fMarginLeft)
    {
        float fPreferedWidth = LayoutUtility.GetPreferredWidth(p_rTextRectTransform);
        p_rTextRectTransform.sizeDelta = new Vector2(fPreferedWidth, 150f);

        Vector3 vOrgPosition = p_rTextRectTransform.localPosition;
        vOrgPosition.x = p_fMarginLeft;

        p_rTextRectTransform.localPosition = vOrgPosition;

        return fPreferedWidth;
    }

    void drawPlayerImage(string p_sPlayerImageName) {
        try
        {
            this.m_goPlayer = Instantiate(Resources.Load("Prefabs/pfPlayer")) as GameObject;
            this.m_goPlayer.SetActive(true);

            Player player = this.m_goPlayer.GetComponent<Player>();

            player.setParent(this.m_goUIContainer.transform);
            player.showPlayer(p_sPlayerImageName);
        }
        catch (Exception ex) { Debug.Log(ex.Message); }
    }

    public void drawBarChart(StatsPlayInfo p_statsPlayInfo) {
        BarChartManager barChartManager = this.m_goBarChartContainer.GetComponent<BarChartManager>();
        barChartManager.initBarChart(p_statsPlayInfo);

//        StartCoroutine("playMainCameraRotation", new Vector3(5f, 3f, 0f));
//        StartCoroutine("playMainCameraPosition", new Vector3(44f, 40f, -80f));
        StartCoroutine("playMainCameraRotation", new Vector3(0f, 0f, 0f));
        StartCoroutine("playMainCameraPosition", new Vector3(50f, 32f, -80f));

        
    }

    public void drawLineChart(StatsPlayInfo p_statsPlayInfo) {
        LineChartManager lineChartManager = this.m_goLineChartContainer.GetComponent<LineChartManager>();
        lineChartManager.initLineChart(p_statsPlayInfo);

        //StartCoroutine("playMainCameraRotation", new Vector3(5f, 3f, 0f));
        //StartCoroutine("playMainCameraPosition", new Vector3(44f, 40f, -80f));
        StartCoroutine("playMainCameraRotation", new Vector3(0f, 0f, 0f));
        StartCoroutine("playMainCameraPosition", new Vector3(50f, 32f, -80f));
    }

    public void drawPieChart(StatsPlayInfo p_statsPlayInfo) {
        PieChartManager pieChartManager = this.m_goPieChartContainer.GetComponent<PieChartManager>();
        pieChartManager.initPieChart(p_statsPlayInfo);

        //StartCoroutine("playMainCameraRotation", new Vector3(5f, 3f, 0f));
        //StartCoroutine("playMainCameraPosition", new Vector3(44f, 40f, -80f));
        StartCoroutine("playMainCameraRotation", new Vector3(0f, 0f, 0f));
        StartCoroutine("playMainCameraPosition", new Vector3(50f, 32f, -80f));
    }

    public void drawStatsChart(StatsPlayInfo p_statsPlayInfo)
    {
        StatsChartManager statsChartManager = this.m_goStatsChartContainer.GetComponent<StatsChartManager>();
        statsChartManager.initStatsChart(p_statsPlayInfo.data);
    }

	public void drawVisualStats(StatsPlayInfo p_statsPlayInfo)
	{
		VisualStatsManager visualStatsManager = this.m_goVisualStatsContainer.GetComponent<VisualStatsManager>();
		visualStatsManager.initVisualStats(p_statsPlayInfo);
	}

#endregion Draw 게임 오브젝트
    
    IEnumerator playMainCameraRotation(Vector3 p_vRotation)
    {
        TweenRotation twTweenRotation = this.m_goMainCamera.GetComponent<TweenRotation>();
        twTweenRotation.from = this.m_goMainCamera.transform.localEulerAngles;
        twTweenRotation.to = p_vRotation;
        twTweenRotation.ResetToBeginning();
        twTweenRotation.PlayForward();

        yield return new WaitForSeconds(0.0f);
    }

    IEnumerator playMainCameraPosition(Vector3 p_vPosition)
    {
        TweenPosition twPosition = this.m_goMainCamera.GetComponent<TweenPosition>();
        twPosition.from = this.m_vMainCameraPosition;
        twPosition.to = p_vPosition;
        twPosition.ResetToBeginning();
        twPosition.PlayForward();

        yield return new WaitForSeconds(0.0f);
    }
}
