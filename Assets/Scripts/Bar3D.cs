﻿using UnityEngine;
using System.Collections;

public class Bar3D : MonoBehaviour
{
    private string m_sTitle;
    private string m_sScore;
    private Material m_mMarerial;
    private float m_fPositionX;
    private float m_fScaleY;

    void Awake()
    {

    }

    public void setParent(Transform _tParent)
    {
        transform.parent = _tParent;
    }

    public void showBar(float _fPositionX, float _fScaleY, Material _mMarerial, StatsPlayInfoItem p_statsPlayInfoItem, bool bShowStar = false)
    {
        this.m_fPositionX = _fPositionX;
        this.m_fScaleY = _fScaleY;
        this.m_mMarerial = _mMarerial;
        this.m_sTitle = p_statsPlayInfoItem.x_title1;
        this.m_sScore = p_statsPlayInfoItem.x_title2;

        gameObject.GetComponent<Renderer>().material = _mMarerial;

        StartCoroutine("playScaleUpBar", bShowStar);
        StartCoroutine("playChangePositionBar");
        StartCoroutine("playChangePositionPointLight");
    }

    IEnumerator playScaleUpBar(bool bShowStar)
    {
        Vector3 vOrgScale = transform.localScale;

        TweenScale twScale = UITweener.Begin<TweenScale>(transform.gameObject, 1.5f);
        twScale.from = vOrgScale;
        twScale.to = new Vector3(vOrgScale.x, this.m_fScaleY, vOrgScale.z);
        twScale.ResetToBeginning();
        twScale.PlayForward();

        yield return new WaitForSeconds(0.5f);

        this.showBarTitle(bShowStar);
    }

    IEnumerator playChangePositionBar()
    {
        float fScaleY = this.m_fScaleY / 2;

        Vector3 vOrgPosition = new Vector3(this.m_fPositionX, 0.0f, 0.0f);
        Vector3 vEndPosition = new Vector3(vOrgPosition.x, fScaleY, vOrgPosition.z);

        transform.localPosition = vOrgPosition;

        TweenPosition twPosition = TweenPosition.Begin<TweenPosition>(gameObject, 1.5f);
        twPosition.from = vOrgPosition;
        twPosition.to = vEndPosition;
        twPosition.ResetToBeginning();
        twPosition.PlayForward();

        yield return new WaitForSeconds(0.0f);
    }

    public void showBarTitle(bool bShowStar = false)
    {
        GameObject goBarTitle = Instantiate(Resources.Load("Prefabs/pfBarScoreTitle")) as GameObject;
        BarScoreTitle barTitle = goBarTitle.GetComponent<BarScoreTitle>();
        barTitle.setParent(transform.parent);
        barTitle.setPosition(new Vector3(this.m_fPositionX, this.m_fScaleY + 8f, transform.localPosition.z));
        barTitle.setRotation();
        barTitle.setTitle(this.m_sTitle, this.m_mMarerial);
        barTitle.setScore(this.m_sScore, this.m_mMarerial);
        if (bShowStar) barTitle.setStar();
    }

    IEnumerator playChangePositionPointLight()
    {
        GameObject goPointLight = Instantiate(Resources.Load("Prefabs/pfBarPointLight")) as GameObject;
        goPointLight.transform.SetParent(transform.parent);
        goPointLight.transform.localEulerAngles = new Vector3(15f, 0f, 0f);

        Vector3 vOrgPosition = new Vector3(this.m_fPositionX - 4f, 0.0f, -7f);
        Vector3 vEndPosition = new Vector3(vOrgPosition.x, this.m_fScaleY - 1f, vOrgPosition.z);

        transform.localPosition = vOrgPosition;

        TweenPosition twPosition = TweenPosition.Begin<TweenPosition>(goPointLight, 1.5f);
        twPosition.from = vOrgPosition;
        twPosition.to = vEndPosition;
        twPosition.ResetToBeginning();
        twPosition.PlayForward();

        yield return new WaitForSeconds(0.0f);
    }

    void OnDisable()
    {

    }
}
