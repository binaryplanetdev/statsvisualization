﻿using UnityEngine;
using System.Collections;

public class BarScoreTitle : MonoBehaviour
{
    public GameObject m_goTitle;
    public GameObject m_goScore;
    public GameObject m_goStar;

    public int nRotationCnt = 0;

    public void setParent(Transform _tParent)
    {
        transform.parent = _tParent;
    }

    public void setPosition(Vector3 _position)
    {
        transform.localPosition = _position;
    }

    public void setTitle(string _sTitle, Material _mMaterial)
    {
        TextMesh childrenComponent = this.m_goTitle.GetComponent<TextMesh>();
        childrenComponent.text = _sTitle;
        childrenComponent.color = _mMaterial.color;

        this.m_goTitle.AddComponent<AutoTyping>();
    }

    public void setScore(string _sScore, Material _mMaterial)
    {
        TextMesh childrenComponent = this.m_goScore.GetComponent<TextMesh>();
        childrenComponent.text = _sScore;
        childrenComponent.color = _mMaterial.color;

        this.m_goScore.AddComponent<AutoTyping>();
    }

    public void setStar()
    {
        this.m_goStar.SetActive(true);
        StartCoroutine("playScaleUpStar");
        StartCoroutine("playRotationStar");

        Debug.Log(-400 + 16f * m_goStar.transform.localPosition.y);

    }

    IEnumerator playScaleUpStar()
    {
        //AudioSource.PlayClipAtPoint(Resources.Load("Sounds/sound_star") as AudioClip, transform.position, 1.0f);

        Vector3 vOrgScale = m_goStar.transform.localScale;

        TweenScale twScale = UITweener.Begin<TweenScale>(m_goStar, 1f);
        twScale.from = vOrgScale;
        twScale.to = new Vector3(6, 6, vOrgScale.z);

        twScale.ResetToBeginning();
        twScale.PlayForward();

        yield return new WaitForSeconds(0.0f);
    }

    IEnumerator playRotationStar()
    {
        Quaternion vOrgRotation = m_goStar.transform.localRotation;

        TweenRotation twRotate = UITweener.Begin<TweenRotation>(m_goStar, 3f);
        twRotate.from = new Vector3(vOrgRotation.x, vOrgRotation.y, 360f/*180f*/);
        twRotate.to = new Vector3(vOrgRotation.x, vOrgRotation.y, 0f);

        twRotate.ResetToBeginning();
        twRotate.PlayForward();

        yield return new WaitForSeconds(3f);

        if(++nRotationCnt < 2)
            StartCoroutine("playRotationStar");
    }

    public void setRotation()
    {
        transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
    }

    void OnDisable()
    {

    }
}
