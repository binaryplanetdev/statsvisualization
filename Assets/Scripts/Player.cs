﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Player : MonoBehaviour {
    public void setParent(Transform _tParent) {
        transform.SetParent(_tParent);
    }

    public void showPlayer(string p_sPlayerImageName) {
        StartCoroutine("loadPlayerImage", p_sPlayerImageName);
    }

    public void hidePlayer() {
        gameObject.SetActive(false);

        try
        {
            Sprite playerSprite = gameObject.GetComponent<Image>().sprite;
            Texture2D playerTexture = playerSprite.texture;

            Destroy(playerTexture);
            Destroy(playerSprite);
        }
        catch { }

        try
        {
            Destroy(gameObject);
        }
        catch { }
    }

    IEnumerator playChangePositionPlayer() {
        TweenPosition twPosition = gameObject.GetComponent<TweenPosition>();
        twPosition.from = new Vector3(1000f, 0f, 0f);
        twPosition.to = new Vector3(0f, 0f, 0f);

        twPosition.ResetToBeginning();
        twPosition.PlayForward();

        yield return new WaitForSeconds(0.0f);
    }

    IEnumerator loadPlayerImage(string p_sFileName)
    {
        WWW www = new WWW(StatVisualization.PLAYER_IMAGE_PATH + p_sFileName);

        yield return www;

        try
        {
            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.Log("ERROR : " + www.error);
            }
            else if (www.size > 0)
            {
                Texture2D tPlayerTexture = www.texture;
                Rect rPlayerTextureRect = new Rect(0f, 0f, tPlayerTexture.width, tPlayerTexture.height);
                Image playerImage = gameObject.GetComponent<Image>();

                playerImage.sprite = Sprite.Create(tPlayerTexture, rPlayerTextureRect, new Vector2(0.5f, 0.5f));

                playerImage.rectTransform.localScale = new Vector3(1f, 1f, 1f);
                
                StartCoroutine("playChangePositionPlayer");
            }
        }
        catch
        {
            Debug.Log("ERROR while fetching player image!");
        }
    }

    void OnDisable()
    {
        //DestroyIme(gameObject);
    }
}
