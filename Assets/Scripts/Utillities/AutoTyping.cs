﻿using UnityEngine;
using System.Collections;

public class AutoTyping : MonoBehaviour {
    public float typingTime = 1f;
    public float m_fStartDelayTime = 0.3f;

    private string m_sText;
    private TextMesh m_tmTextMesh;

    private float m_fTypingSpeed;

	void Start() {
        
    }
	
	IEnumerator playTypingText() {
        yield return new WaitForSeconds(this.m_fStartDelayTime);

        char[] letter = this.m_sText.ToCharArray();

        for(int i = 0; i < letter.Length; i++) {
            this.m_tmTextMesh.text += letter[i];

            yield return new WaitForSeconds(this.m_fTypingSpeed);
        }
    }

    void OnEnable() {
        this.m_tmTextMesh = transform.GetComponent<TextMesh>();
        this.m_sText = this.m_tmTextMesh.text;

        this.m_tmTextMesh.text = "";

        this.m_fTypingSpeed = (this.typingTime - this.m_fStartDelayTime) / this.m_sText.Length;

        StartCoroutine("playTypingText");
    }
}
