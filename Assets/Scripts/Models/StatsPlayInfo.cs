﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class StatsPlayInfo
{
    public string pl_no = "14";

    public string pl_nm = "최정";

    public string pl_img_path = "";

    public string pl_pos = "pitcher";

    /// <summary>
    /// 차트 타입 (bar, pie, line, stats)
    /// </summary>
    public string ctype = "bar";

    /// <summary>
    /// 타이틀
    /// </summary>
    public string title = "초구타격성공률";

    public double y_axis_min = 0.22;

    public double y_axis_max = 0.4;

    public int x_axis_cnt = 3;

    public StatsPlayInfoItem[] data;
}

