﻿using UnityEngine;
using System.Collections;
using CP.ProChart;

public class PieChartManager : MonoBehaviour
{
    public GameObject m_goPieChartTitleContainer;

    private Material[] m_mMaterials;

    GameObject m_goGraphContainer = null;

    private int m_nPieGap = 7;

    public void initPieChart(StatsPlayInfo p_statsPlayInfo)
    {
        gameObject.SetActive(true);

        this.m_mMaterials = new Material[5];
        this.m_mMaterials[0] = Resources.Load("Materials/matPieGreen", typeof(Material)) as Material;
        this.m_mMaterials[1] = Resources.Load("Materials/matPieYellow", typeof(Material)) as Material;
        this.m_mMaterials[2] = Resources.Load("Materials/matPieOrange", typeof(Material)) as Material;
        this.m_mMaterials[3] = Resources.Load("Materials/matPieRed", typeof(Material)) as Material;
        this.m_mMaterials[4] = Resources.Load("Materials/matPieBlue", typeof(Material)) as Material;

        this.createGraphContainer();

        this.drawPieChartTitle(p_statsPlayInfo.title);

        StartCoroutine("drawPieChart", p_statsPlayInfo);
    }

    void createGraphContainer()
    {
        this.m_goGraphContainer = new GameObject();
        this.m_goGraphContainer.name = "GraphContainer";
        this.m_goGraphContainer.transform.SetParent(transform);
        this.m_goGraphContainer.transform.localPosition = new Vector3(0f, 0f, 0f);
        this.m_goGraphContainer.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
    }

    void drawPieChartTitle(string p_sTitle)
    {
        Transform tPieChartTitle = this.m_goPieChartTitleContainer.transform.FindChild("PieChartTitle");
        TextMesh tPieTitleTextMesh = tPieChartTitle.GetComponent<TextMesh>();
        tPieTitleTextMesh.text = p_sTitle;

        Transform tPieChartTitleBg = this.m_goPieChartTitleContainer.transform.FindChild("PieChartTitleBg");
        Vector3 vOrgScale = tPieChartTitleBg.localScale;
        vOrgScale.x = (float)(((tPieTitleTextMesh.fontSize * 0.09) * p_sTitle.Length) * tPieChartTitle.localScale.x) + 2f;

        tPieChartTitleBg.localScale = vOrgScale;

        this.m_goPieChartTitleContainer.SetActive(true);
    }

    IEnumerator drawPieChart(StatsPlayInfo p_statsPlayInfo)
    {
        yield return new WaitForSeconds(0.5f);

        float fTotalValue = 0f;
        int nTotalPieGap = this.m_nPieGap * p_statsPlayInfo.x_axis_cnt;
        int nTotalChartSize = 360 - nTotalPieGap;

        foreach (StatsPlayInfoItem item in p_statsPlayInfo.data)
        {
            fTotalValue += (float)item.y_value;
        }

        float fStartAngle = 0f;
        for (int i = 0; i < p_statsPlayInfo.x_axis_cnt; i++)
        {
            float fChartSize = (float)(nTotalChartSize * (p_statsPlayInfo.data[i].y_value / fTotalValue));

            this.createPie(i, fStartAngle, fChartSize, p_statsPlayInfo.data[i]);

            fStartAngle += fChartSize + this.m_nPieGap;

            yield return new WaitForSeconds(0.1f);
        }
    }

    void createPie(int _nSeq, float p_fStartAngle, float p_fChartSize, StatsPlayInfoItem p_statsPlayInfoItem)
    {
        GameObject goPie = Instantiate(Resources.Load("Prefabs/pfPie3D")) as GameObject;
        Pie3D pie = goPie.GetComponent<Pie3D>();

        pie.setParent(this.m_goGraphContainer.transform);
        pie.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
        pie.transform.localPosition = new Vector3(0f, 0f, 0f);

        pie.showPie(p_fChartSize, "pie" + (_nSeq + 1), this.m_mMaterials[_nSeq % this.m_mMaterials.Length], p_statsPlayInfoItem.x_title1, p_statsPlayInfoItem.x_title2 + "", p_fStartAngle);
    }

    public void removeChart()
    {
        if (this.m_goGraphContainer != null)
        {
            Destroy(this.m_goGraphContainer);
        }

        this.m_goPieChartTitleContainer.SetActive(false);
    }
}
