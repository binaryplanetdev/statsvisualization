﻿    using UnityEngine;
using System.Collections;
using CP.ProChart;

public class Pie3D : MonoBehaviour
{
    private PieChartMeshCustom m_oPieChart = null;

    private float speed = 150.0F;
    private float startTime;
    private float journeyLength;

    private float m_fChartSize = 0f;
    private float m_fHalfRadius = 15f;

    private bool m_bIsStartAnimation;

    void Awake()
    {
        this.m_bIsStartAnimation = false;
    }

    public void setParent(Transform _tParent)
    {
        transform.SetParent(_tParent);
    }

    public void showPie(float p_fChartSize, string p_sLayerName, Material p_mMarerial, string p_sTitle, string p_sValue, float p_fStartAngle)
    {
        this.m_fChartSize = p_fChartSize;

        Transform pieChild = transform.FindChild("pie");
        Transform titleChild = pieChild.FindChild("title");
        Transform valueChild = pieChild.FindChild("value");
        Transform reflectionChild = titleChild.FindChild("pieReflection");
        pieChild.localEulerAngles = new Vector3(50f, 0f, 0f);

        this.setPie(pieChild, p_fStartAngle, p_mMarerial);

        this.setPieTitle(titleChild, p_sTitle, p_fStartAngle, p_sLayerName);
        this.setPieValue(valueChild, p_sValue, p_fStartAngle, p_sLayerName);

        //reflectionChild.localPosition = _vReflectionPosition;
        ReflectionProbe reflectionProbe = reflectionChild.GetComponent<ReflectionProbe>();
        reflectionProbe.cullingMask = 1 << LayerMask.NameToLayer(p_sLayerName);

        gameObject.SetActive(true);

        startTime = Time.time;

        this.m_bIsStartAnimation = true;
    }

    void setPieTitle(Transform p_tTitleChild, string p_sTitle, float p_fStartAngle, string p_sLayerName)
    {
        p_tTitleChild.gameObject.layer = LayerMask.NameToLayer(p_sLayerName);

        TextMesh titleText = p_tTitleChild.GetComponent<TextMesh>();
        titleText.text = p_sTitle;

        p_tTitleChild.localPosition = this.getPieTitlePosition(p_fStartAngle);
        p_tTitleChild.localEulerAngles = new Vector3(-90f, 0f, 0f);

        p_tTitleChild.gameObject.AddComponent<AutoTyping>();
        p_tTitleChild.gameObject.GetComponent<AutoTyping>().m_fStartDelayTime = 0.3f;
        p_tTitleChild.gameObject.GetComponent<AutoTyping>().typingTime = 1f;
    }

    void setPieValue(Transform p_tValueChild, string p_sValue, float p_fStartAngle, string p_sLayerName)
    {
        p_tValueChild.gameObject.layer = LayerMask.NameToLayer(p_sLayerName);

        TextMesh titleText = p_tValueChild.GetComponent<TextMesh>();
        titleText.text = p_sValue;

        p_tValueChild.localPosition = this.getPieValuePosition(p_fStartAngle);
        p_tValueChild.localEulerAngles = new Vector3(-90f, 0f, 0f);

        p_tValueChild.gameObject.AddComponent<AutoTyping>();
        p_tValueChild.gameObject.GetComponent<AutoTyping>().m_fStartDelayTime = 0.3f;
        p_tValueChild.gameObject.GetComponent<AutoTyping>().typingTime = 1f;
    }

    Vector3 getPieTitlePosition(float p_fStartAngle)
    {
        float fTargetDegree = ((p_fStartAngle + (p_fStartAngle + this.m_fChartSize)) / 2) * -1;
        Vector3 vPieTitlePosition = new Vector3(0f, 0f, -8f);
        Vector3 vDistance = Vector3.up * m_fHalfRadius;
        Quaternion qRotate = Quaternion.Euler(0f, 0f, fTargetDegree);

        Vector3 vTarget = qRotate * vDistance;
        vPieTitlePosition = vPieTitlePosition + vTarget;

        return vPieTitlePosition;
    }

    Vector3 getPieValuePosition(float p_fStartAngle)
    {
        float fTargetDegree = ((p_fStartAngle + (p_fStartAngle + this.m_fChartSize)) / 2) * -1;
        Vector3 vPieTitlePosition = new Vector3(0f, 0f, -4f);
        Vector3 vDistance = Vector3.up * m_fHalfRadius;
        Quaternion qRotate = Quaternion.Euler(0f, 0f, fTargetDegree);

        Vector3 vTarget = qRotate * vDistance;
        vPieTitlePosition = vPieTitlePosition + vTarget;

        return vPieTitlePosition;
    }

    void setPie(Transform p_tPieChild, float p_fStartAngle, Material p_mMaterial)
    {
        p_tPieChild.gameObject.AddComponent<PieChartMeshCustom>();
        p_tPieChild.gameObject.GetComponent<MeshRenderer>().material = p_mMaterial;

        this.m_oPieChart = p_tPieChild.gameObject.GetComponent<PieChartMeshCustom>();

        ChartData1D cdDataSet = new ChartData1D();
        cdDataSet[0] = 50;
        this.m_oPieChart.SetValues(ref cdDataSet);

        this.m_oPieChart.mode_3d = true;
        this.m_oPieChart.size = new Vector3(45f, 45f, 5f);
        this.m_oPieChart.StartAngle = p_fStartAngle;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.m_bIsStartAnimation)
        {
            float distCovered = (Time.time - startTime) * speed; //속력 v = m/s 1초에 10움직임 한프레임당 1움직인다고하면
            float fracJourney = distCovered / this.m_fChartSize;

            this.m_oPieChart.ChartSize = Mathf.Lerp(0, this.m_fChartSize, fracJourney);

            if (this.m_oPieChart.ChartSize == this.m_fChartSize)
            {
                this.m_bIsStartAnimation = false;
            }
        }
    }

    void OnDisable()
    {
    }
}
