﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class StatsChartManager : MonoBehaviour {

    //GameObject[] m_goPanels = null;
    public GameObject m_goStatsChartSeasonTitle;
    GameObject m_goGraphContainer = null;

	int m_nTotalItemCount;

    // Use this for initialization
    void Start () {
		this.m_nTotalItemCount = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void initStatsChart(StatsPlayInfoItem[] p_statsPlayInfoItems)
    {
        gameObject.SetActive(true);

        this.createGraphContainer();

        this.drawStatsChartSeasonTitle();

		StartCoroutine("drawStatsChart", p_statsPlayInfoItems);
    }

    void createGraphContainer()
    {
        this.m_goGraphContainer = new GameObject();
        this.m_goGraphContainer.name = "GraphContainer";
        this.m_goGraphContainer.transform.SetParent(transform);
        this.m_goGraphContainer.transform.localPosition = new Vector3(0f, 0f, 0f);
		this.m_goGraphContainer.transform.localScale = new Vector3(1f, 1f, 1f);
        this.m_goGraphContainer.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
    }

	IEnumerator drawStatsChart(StatsPlayInfoItem[] p_statsPlayInfoItems)
    {
        yield return new WaitForSeconds(0.0f);

        /// hsshin 사운드 추가
        this.transform.GetComponent<AudioSource>().PlayDelayed(0.5f);

        this.m_nTotalItemCount = p_statsPlayInfoItems.Length;
		int i = 0;
		foreach (StatsPlayInfoItem item in p_statsPlayInfoItems) {
			GameObject goStatsPannel = Instantiate(Resources.Load("Prefabs/pfStatsPanel")) as GameObject;
			goStatsPannel.name = "pfStatsPanel_" + i;
			StatsPannel statsPannel = goStatsPannel.GetComponent<StatsPannel>();

			float fPositionY = 500 + (-160 * (i + 1));
			statsPannel.setParent(this.m_goGraphContainer.transform);
			statsPannel.showStatsPannel (item, fPositionY);

			i++;

			yield return new WaitForSeconds(0.2f/*0.25f*/);
		}
    }

    void drawStatsChartSeasonTitle()
    {
        try
        {
            this.m_goStatsChartSeasonTitle.SetActive(true);
            StartCoroutine("playChangeAlpha", this.m_goStatsChartSeasonTitle);
        }
        catch (Exception ex) { Debug.Log(ex.Message); }
    }

    public void removeChart()
    {
		try
        {
			if (this.m_goGraphContainer != null) {

                for (int i = 0; i < this.m_nTotalItemCount; i++) {
                    try
                    {
                        Transform stastPannel = this.m_goGraphContainer.transform.Find("pfStatsPanel_" + i);
                        Transform tfStatsValue1 = stastPannel.Find("Stats1");
                        Transform tfStatsValue2 = stastPannel.Find("Stats2");
                        Transform tfStatsValue3 = stastPannel.Find("Stats3");
                        Transform tfStatsValue4 = stastPannel.Find("Stats4");
                        Transform tfStatsValue5 = stastPannel.Find("Stats5");

                        Image valueimg1 = tfStatsValue1.transform.Find("Team").GetComponentInChildren<Image>();
                        Image valueimg2 = tfStatsValue2.transform.Find("Team").GetComponentInChildren<Image>();
                        Image valueimg3 = tfStatsValue3.transform.Find("Team").GetComponentInChildren<Image>();
                        Image valueimg4 = tfStatsValue4.transform.Find("Team").GetComponentInChildren<Image>();
                        Image valueimg5 = tfStatsValue5.transform.Find("Team").GetComponentInChildren<Image>();

                        try
                        {
                            Destroy(valueimg1.sprite.texture);
                            Destroy(valueimg1.sprite);
                        }
                        catch { }
                        try
                        {
                            Destroy(valueimg2.sprite.texture);
                            Destroy(valueimg2.sprite);
                        }
                        catch { }

                        try
                        {
                            Destroy(valueimg3.sprite.texture);
                            Destroy(valueimg3.sprite);
                        }
                        catch { }

                        try
                        {
                            Destroy(valueimg4.sprite.texture);
                            Destroy(valueimg4.sprite);
                        }
                        catch { }

                        try
                        {
                            Destroy(valueimg5.sprite.texture);
                            Destroy(valueimg5.sprite);
                        }
                        catch { }
                    }
                    catch { }
				}

                try
                {
                    Destroy(this.m_goGraphContainer);
                }
                catch { }

            }

			this.m_goStatsChartSeasonTitle.SetActive(false);
        }
		catch (Exception ex) { Debug.Log(ex.Message); }
    }
    
    int indexAlphaAnimation = 0;
    IEnumerator playChangeAlpha(GameObject go)
    {
        CanvasRenderer[] arrCr = go.GetComponentsInChildren<CanvasRenderer>();

        if (arrCr != null)
        {
            foreach (CanvasRenderer cr in arrCr)
                cr.SetAlpha(0.0f);

            yield return new WaitForSeconds(0.0f + ((float)(indexAlphaAnimation++) * 0.02f));

            if (arrCr != null)
            {

                float start = Time.time;
                while (arrCr[0].GetAlpha() != 1.0f)
                {
                    float elapsed = Time.time - start;
                    float normalisedTime = Mathf.Clamp((elapsed / 1.0f) * Time.deltaTime, 0, 1);
                    foreach (CanvasRenderer cr in arrCr)
                        cr.SetAlpha(Mathf.Lerp(cr.GetAlpha(), 1.0f, normalisedTime));

                    yield return 0;
                }
            }
        }

        yield return true;
    }
}
