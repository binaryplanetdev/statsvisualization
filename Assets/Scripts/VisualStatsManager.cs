﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class VisualStatsManager : MonoBehaviour {
	GameObject m_goGraphContainer = null;

	// Use this for initialization
	void Start () {
	
	}

	public void initVisualStats(StatsPlayInfo p_statsPlayInfo)
	{
		gameObject.SetActive(true);

		this.createGraphContainer ();

        /// hsshin 사운드 추가
        this.transform.GetComponent<AudioSource>().PlayDelayed(0.5f);

		if (p_statsPlayInfo.ctype == "futures_defence")
		{
			StartCoroutine("getFuturesDefence", p_statsPlayInfo);
		}
		else if (p_statsPlayInfo.ctype == "futures_offence")
		{
			StartCoroutine("getFuturesOffence", p_statsPlayInfo);
		}
		else if (p_statsPlayInfo.ctype == "pinch_hitter")
		{
			StartCoroutine("getPinchHitter", p_statsPlayInfo);
		}
		else if (p_statsPlayInfo.ctype == "steal")
		{
			StartCoroutine("getSteal", p_statsPlayInfo);
		}
		else if (p_statsPlayInfo.ctype == "advance_base")
		{
			StartCoroutine("getAdvanceBase", p_statsPlayInfo);
		}
		else if (p_statsPlayInfo.ctype == "ball_type_left")
		{
			StartCoroutine("getBallTypeLeft", p_statsPlayInfo);
		}
		else if (p_statsPlayInfo.ctype == "ball_type_right")
		{
			StartCoroutine("getBallTypeRight", p_statsPlayInfo);
		}
	}

	void createGraphContainer()
	{
		this.m_goGraphContainer = new GameObject();
		this.m_goGraphContainer.name = "GraphContainer";
		this.m_goGraphContainer.transform.SetParent(transform);
		this.m_goGraphContainer.transform.localPosition = new Vector3(-50f, 0f, 0f);
		this.m_goGraphContainer.transform.localScale = new Vector3(0.95f, 0.95f, 1f);
		this.m_goGraphContainer.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
	}

	IEnumerator getFuturesDefence(StatsPlayInfo p_statsPlayInfo)
	{
		GameObject goFuturesDefence = Instantiate(Resources.Load("Prefabs/pfFuturesLeagueDefence")) as GameObject;		
		goFuturesDefence.SetActive (true);
		goFuturesDefence.transform.SetParent(this.m_goGraphContainer.transform);
		goFuturesDefence.transform.localPosition = new Vector3(-1920f, 0f, 0f);
		goFuturesDefence.transform.localScale = new Vector3 (1f, 1f, 1f);

        Debug.Log("OPEN : Prefabs/pfFuturesLeagueDefence" );

		Transform text1 = goFuturesDefence.transform.FindChild ("txt1");
		Transform text2 = goFuturesDefence.transform.FindChild ("txt2");
		Transform text3 = goFuturesDefence.transform.FindChild ("txt3");
		Transform text4 = goFuturesDefence.transform.FindChild ("txt4");
		Transform text5 = goFuturesDefence.transform.FindChild ("txt5");

		StatsPlayInfoItem[] statsPlayInfoItems = p_statsPlayInfo.data;

		text1.GetComponent<Text> ().text = statsPlayInfoItems[0].y_value1;
        Debug.Log("y_value1 : " + statsPlayInfoItems[0].y_value1);

		text2.GetComponent<Text> ().text = statsPlayInfoItems[0].y_value2;
		text3.GetComponent<Text> ().text = statsPlayInfoItems[0].y_value3;
		text4.GetComponent<Text> ().text = statsPlayInfoItems[0].y_value4;
		text5.GetComponent<Text> ().text = statsPlayInfoItems[0].y_value5;

		TweenPosition twPosition = goFuturesDefence.GetComponent<TweenPosition>();
		twPosition.from = new Vector3(-1920f, 0f, 0f);
		twPosition.to = new Vector3(0f, 0f, 0f);
		twPosition.ResetToBeginning();
		twPosition.PlayForward();

		yield return new WaitForSeconds(0f);
	}

	IEnumerator getFuturesOffence(StatsPlayInfo p_statsPlayInfo)
	{
		GameObject goFuturesOffence = Instantiate(Resources.Load("Prefabs/pfFuturesLeagueOffence")) as GameObject;		
		goFuturesOffence.SetActive (true);
		goFuturesOffence.transform.SetParent(this.m_goGraphContainer.transform);
		goFuturesOffence.transform.localPosition = new Vector3(-1920f, 0f, 0f);
		goFuturesOffence.transform.localScale = new Vector3 (1f, 1f, 1f);

		Transform text1 = goFuturesOffence.transform.FindChild ("txt1");
		Transform text2 = goFuturesOffence.transform.FindChild ("txt2");
		Transform text3 = goFuturesOffence.transform.FindChild ("txt3");
		Transform text4 = goFuturesOffence.transform.FindChild ("txt4");
		Transform text5 = goFuturesOffence.transform.FindChild ("txt5");

		StatsPlayInfoItem[] statsPlayInfoItems = p_statsPlayInfo.data;

		text1.GetComponent<Text> ().text = statsPlayInfoItems[0].y_value1;
		text2.GetComponent<Text> ().text = statsPlayInfoItems[0].y_value2;
		text3.GetComponent<Text> ().text = statsPlayInfoItems[0].y_value3;
		text4.GetComponent<Text> ().text = statsPlayInfoItems[0].y_value4;
		text5.GetComponent<Text> ().text = statsPlayInfoItems[0].y_value5;

		TweenPosition twPosition = goFuturesOffence.GetComponent<TweenPosition>();
		twPosition.from = new Vector3(-1920f, 0f, 0f);
		twPosition.to = new Vector3(0f, 0f, 0f);
		twPosition.ResetToBeginning();
		twPosition.PlayForward();

		yield return new WaitForSeconds(0f);
	}

	IEnumerator getPinchHitter(StatsPlayInfo p_statsPlayInfo)
	{
		GameObject goPinchHitter = Instantiate(Resources.Load("Prefabs/pfPinchHitter")) as GameObject;		
		goPinchHitter.SetActive (true);
		goPinchHitter.transform.SetParent(this.m_goGraphContainer.transform);
		goPinchHitter.transform.localPosition = new Vector3(-1920f, 0f, 0f);
		goPinchHitter.transform.localScale = new Vector3 (1f, 1f, 1f);

		Transform score_txt = goPinchHitter.transform.FindChild ("score_txt");

		StatsPlayInfoItem[] statsPlayInfoItems = p_statsPlayInfo.data;

		score_txt.GetComponent<Text> ().text = statsPlayInfoItems[0].y_value1;

		TweenPosition twPosition = goPinchHitter.GetComponent<TweenPosition>();
		twPosition.from = new Vector3(-1920f, 0f, 0f);
		twPosition.to = new Vector3(0f, 0f, 0f);
		twPosition.ResetToBeginning();
		twPosition.PlayForward();

		yield return new WaitForSeconds(0f);
	}

	IEnumerator getSteal(StatsPlayInfo p_statsPlayInfo)
	{
		GameObject goSteal = Instantiate(Resources.Load("Prefabs/pfSteal")) as GameObject;		
		goSteal.SetActive (true);
		goSteal.transform.SetParent(this.m_goGraphContainer.transform);
		goSteal.transform.localPosition = new Vector3(-1920f, 0f, 0f);
		goSteal.transform.localScale = new Vector3 (1f, 1f, 1f);

		Transform score_txt = goSteal.transform.FindChild ("score_txt");

		StatsPlayInfoItem[] statsPlayInfoItems = p_statsPlayInfo.data;

        score_txt.GetComponent<Text>().text = statsPlayInfoItems[0].y_value1;

		TweenPosition twPosition = goSteal.GetComponent<TweenPosition>();
		twPosition.from = new Vector3(-1920f, 0f, 0f);
		twPosition.to = new Vector3(0f, 0f, 0f);
		twPosition.ResetToBeginning();
		twPosition.PlayForward();

		yield return new WaitForSeconds(0f);
	}

	IEnumerator getAdvanceBase(StatsPlayInfo p_statsPlayInfo)
	{
		GameObject goAdvanceBase = Instantiate(Resources.Load("Prefabs/pfAdvanceBase")) as GameObject;		
		goAdvanceBase.SetActive (true);
		goAdvanceBase.transform.SetParent(this.m_goGraphContainer.transform);
		goAdvanceBase.transform.localPosition = new Vector3(-1920f, 0f, 0f);
		goAdvanceBase.transform.localScale = new Vector3 (1f, 1f, 1f);

		Transform score_txt = goAdvanceBase.transform.FindChild ("score_txt");

		StatsPlayInfoItem[] statsPlayInfoItems = p_statsPlayInfo.data;

        string sText = statsPlayInfoItems[0].y_value1;
        try
        {
            sText = String.Format("{0:F2}%", Convert.ToDouble(sText));
        }
        catch { }

        score_txt.GetComponent<Text>().text = sText;

		TweenPosition twPosition = goAdvanceBase.GetComponent<TweenPosition>();
		twPosition.from = new Vector3(-1920f, 0f, 0f);
		twPosition.to = new Vector3(0f, 0f, 0f);
		twPosition.ResetToBeginning();
		twPosition.PlayForward();

		yield return new WaitForSeconds(0f);
	}

	IEnumerator getBallTypeLeft(StatsPlayInfo p_statsPlayInfo)
	{
		GameObject goBallTypeLeft = Instantiate(Resources.Load("Prefabs/pfBallTypeLeft")) as GameObject;		
		goBallTypeLeft.SetActive (true);
		goBallTypeLeft.transform.SetParent(this.m_goGraphContainer.transform);
		goBallTypeLeft.transform.localPosition = new Vector3(-1920f, 0f, 0f);
		goBallTypeLeft.transform.localScale = new Vector3 (1f, 1f, 1f);

		StatsPlayInfoItem[] statsPlayInfoItems = p_statsPlayInfo.data;

		if (statsPlayInfoItems.Length > 0) {
			foreach (StatsPlayInfoItem item in statsPlayInfoItems) {
				try {
					SpriteRenderer spriteRenderer = goBallTypeLeft.transform.FindChild (item.x_title1.ToLower ()).GetComponent<SpriteRenderer> ();
					Color rendererColor = spriteRenderer.color;
					rendererColor.a = 1f;
					spriteRenderer.color = rendererColor;
				} catch {}
			}
		}

		TweenPosition twPosition = goBallTypeLeft.GetComponent<TweenPosition>();
		twPosition.from = new Vector3(-1920f, 0f, 0f);
		twPosition.to = new Vector3(0f, 0f, 0f);
		twPosition.ResetToBeginning();
		twPosition.PlayForward();

		yield return new WaitForSeconds(0f);
	}

	IEnumerator getBallTypeRight(StatsPlayInfo p_statsPlayInfo)
	{
		GameObject goBallTypeRight = Instantiate(Resources.Load("Prefabs/pfBallTypeRight")) as GameObject;		
		goBallTypeRight.SetActive (true);
		goBallTypeRight.transform.SetParent(this.m_goGraphContainer.transform);
		goBallTypeRight.transform.localPosition = new Vector3(-1920f, 0f, 0f);
		goBallTypeRight.transform.localScale = new Vector3 (1f, 1f, 1f);

		StatsPlayInfoItem[] statsPlayInfoItems = p_statsPlayInfo.data;

		if (statsPlayInfoItems.Length > 0) {
			foreach (StatsPlayInfoItem item in statsPlayInfoItems) {
				try {
					SpriteRenderer spriteRenderer = goBallTypeRight.transform.FindChild (item.x_title1.ToLower ()).GetComponent<SpriteRenderer> ();
					Color rendererColor = spriteRenderer.color;
					rendererColor.a = 1f;
					spriteRenderer.color = rendererColor;
				} catch {}
			}
		}

		TweenPosition twPosition = goBallTypeRight.GetComponent<TweenPosition>();
		twPosition.from = new Vector3(-1920f, 0f, 0f);
		twPosition.to = new Vector3(0f, 0f, 0f);
		twPosition.ResetToBeginning();
		twPosition.PlayForward();

		yield return new WaitForSeconds(0f);
	}

	public void removeChart()
	{
		try
		{
			if (this.m_goGraphContainer != null) {
				Destroy(this.m_goGraphContainer);
			}
		}
		catch (Exception ex) { Debug.Log(ex.Message); }
	}
}
