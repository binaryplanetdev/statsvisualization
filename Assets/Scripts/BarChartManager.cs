﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class BarChartManager : MonoBehaviour
{
    public GameObject m_goBarChartTitleContainer;
    public GameObject m_goStarEffect;

    private string[] m_sThreeMaterials = new string[] { "matBlue", "matRed", "matWhite" };
    private string[] m_sTwoMaterials = new string[] { "matRed", "matWhite" };

    private float m_fThreeBarVerticalLineStep = 16f;
    private float m_fThreeBarStartX = 8f;

    private float m_fTwoBarVerticalLineStep = 24f;
    private float m_fTwoBarStartX = 12f;

    private Material[] m_mMaterials;
    private GameObject m_goGraphContainer = null;
    private GameObject m_goGridContainer = null;

    private int m_nDrawHorizontalLineCount = 10;
    private float m_fHorizontalLineStartY = 40f;
    private float m_fHorizontalLineStep = -4f;
    private float m_fVerticalLineStep = 16f;
    private float m_fBarStartX = 8f;
    private float m_fMaxYScale = 42f;


    private float m_fMinBarScore;

    public void initBarChart(StatsPlayInfo p_statsPlayInfo)
    {
        gameObject.SetActive(true);

        //Debug.Log("데이터 개수 : " + p_statsPlayInfo.data.Length);

        this.initSettings(p_statsPlayInfo);
        this.createGraphContainer();
        this.createGridContainer();

        this.drawBarChartTitle(p_statsPlayInfo.title);

        this.drawHorizontalLine((float)p_statsPlayInfo.y_axis_min, (float)p_statsPlayInfo.y_axis_max);
        this.drawVerticalLine(p_statsPlayInfo.x_axis_cnt);

        this.drawBarChart(p_statsPlayInfo);
    }

    private void initSettings(StatsPlayInfo p_statsPlayInfo)
    {
        string[] materialNames = this.m_sThreeMaterials;
        this.m_fVerticalLineStep = this.m_fThreeBarVerticalLineStep;
        this.m_fBarStartX = this.m_fThreeBarStartX;

        if(p_statsPlayInfo.x_axis_cnt == 2)
        {
            materialNames = this.m_sTwoMaterials;
            this.m_fVerticalLineStep = this.m_fTwoBarVerticalLineStep;
            this.m_fBarStartX = this.m_fTwoBarStartX;
        }

        this.m_mMaterials = new Material[p_statsPlayInfo.x_axis_cnt];
        for (int i = 0; i < p_statsPlayInfo.x_axis_cnt; i++)
        {
            this.m_mMaterials[i] = Resources.Load("Materials/" + materialNames[i], typeof(Material)) as Material;
        }

    }

    void createGraphContainer()
    {
        this.m_goGraphContainer = new GameObject();
        this.m_goGraphContainer.name = "GraphContainer";
        this.m_goGraphContainer.transform.SetParent(transform);
        this.m_goGraphContainer.transform.localPosition = new Vector3(0f, 0f, 0f);
        this.m_goGraphContainer.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
    }

    void createGridContainer()
    {
        this.m_goGridContainer = new GameObject();
        this.m_goGridContainer.name = "GridContainer";
        this.m_goGridContainer.transform.SetParent(transform);
        this.m_goGridContainer.transform.localPosition = new Vector3(0f, 0f, 6f);
        this.m_goGridContainer.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
    }

    void drawBarChartTitle(string p_sTitle)
    {
        Transform tBarChartTitle = this.m_goBarChartTitleContainer.transform.FindChild("BarChartTitle");
        TextMesh barTitleTextMesh = tBarChartTitle.GetComponent<TextMesh>();
        barTitleTextMesh.text = p_sTitle;

        Transform tBarChartTitleBg = this.m_goBarChartTitleContainer.transform.FindChild("BarChartTitleBg");
        Vector3 vOrgScale = tBarChartTitleBg.localScale;
        vOrgScale.x = (float)(((barTitleTextMesh.fontSize * 0.09) * p_sTitle.Length) * tBarChartTitle.localScale.x) + 2f;

        tBarChartTitleBg.localScale = vOrgScale;

        this.m_goBarChartTitleContainer.SetActive(true);
    }

    void drawHorizontalLine(float p_fMinScore, float p_fMaxScore)
    {
        float fScoreDesc = ((p_fMaxScore - p_fMinScore) / (m_nDrawHorizontalLineCount - 1));
        float fStartScore = p_fMaxScore;

        for (int i = 0; i < m_nDrawHorizontalLineCount; i++)
        {
            GameObject goHorizontalLine = Instantiate(Resources.Load("Prefabs/pfGridHorizontalLine")) as GameObject;
            goHorizontalLine.transform.SetParent(this.m_goGridContainer.transform);

            goHorizontalLine.GetComponentInChildren<TextMesh>().text = fStartScore.ToString("F2");

            goHorizontalLine.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
            goHorizontalLine.transform.localPosition = new Vector3(0f, m_fHorizontalLineStartY + (i * m_fHorizontalLineStep), 0f);

            fStartScore -= fScoreDesc;

            if (i == m_nDrawHorizontalLineCount - 1)
            {
                Transform tChild = goHorizontalLine.transform.FindChild("line");
                tChild.gameObject.SetActive(false);
            }
        }

        m_fMinBarScore = fStartScore;
    }

    void drawVerticalLine(int p_nScoreCnt)
    {
        for (int i = 0; i < p_nScoreCnt + 1; i++)
        {
            GameObject goVerticalLine = Instantiate(Resources.Load("Prefabs/pfGridVerticalLine")) as GameObject;
            goVerticalLine.transform.SetParent(this.m_goGridContainer.transform);

            goVerticalLine.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
            goVerticalLine.transform.localPosition = new Vector3(i * m_fVerticalLineStep, 22f, 0f);
        }
    }

    void drawBarChart(StatsPlayInfo p_statsPlayInfo)
    {
        float fMaxScore = (float)p_statsPlayInfo.y_axis_max;

        for (int i = 0; i < p_statsPlayInfo.x_axis_cnt; i++)
        {
            StartCoroutine("createBar", new object[] { i, fMaxScore, p_statsPlayInfo.data[i], (i == p_statsPlayInfo.x_axis_cnt -1 ? true : false) });
        }
    }

    IEnumerator createBar(object[] p_oParams)
    {
        yield return new WaitForSeconds(1.0f);

        int seq = (int)p_oParams[0];
        float fMaxScore = (float)p_oParams[1];
        StatsPlayInfoItem statsPlayInfoItem = (StatsPlayInfoItem)p_oParams[2];

        GameObject goBar = Instantiate(Resources.Load("Prefabs/pfBar3D")) as GameObject;
        Bar3D bar = goBar.GetComponent<Bar3D>();

        float fDiff = fMaxScore - m_fMinBarScore;
        float fActualVal = (float)(statsPlayInfoItem.y_value - m_fMinBarScore);

        float fPositionX = this.m_fBarStartX + (m_fVerticalLineStep * seq);
        float fScaleY = (m_fMaxYScale * fActualVal) / fDiff;

        bar.setParent(this.m_goGraphContainer.transform);
        goBar.transform.localEulerAngles = new Vector3(0f, 0f, 0f);

        bar.showBar(fPositionX, fScaleY, this.m_mMaterials[seq], statsPlayInfoItem, (bool)p_oParams[3]);

        if ((bool)p_oParams[3])
        {
            Debug.Log(fScaleY);
            Debug.Log(-350 + (16f * fScaleY));
            m_goStarEffect.transform.localPosition = new Vector3(-200f, -350 + (16f * fScaleY), 0f);
        }

        yield return new WaitForSeconds(0.0f);
    }

    public void removeChart()
    {
        if (this.m_goGraphContainer != null)
        {
            Destroy(this.m_goGraphContainer);
        }

        if (this.m_goGridContainer != null)
        {
            Destroy(this.m_goGridContainer);
        }

        this.m_goBarChartTitleContainer.SetActive(false);
    }

    void OnDisable()
    {

    }
}
