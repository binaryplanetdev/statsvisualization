﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonController : MonoBehaviour
{
    public StatVisualization m_cStatVisualization;
    public GameObject m_btnStats;
    public GameObject m_btnBar3;
    public GameObject m_btnBar2;
    public GameObject m_btnLine;
    public GameObject m_btnPie;
	public GameObject m_btnBallType;
	public GameObject m_btnAdvanceBase;
	public GameObject m_btnFuturesDefence;
	public GameObject m_btnFuturesOffence;
	public GameObject m_btnPinchHitter;
    public GameObject m_btnSteal;
    public GameObject m_btnStop;


    void Awake()
    {
        Button btnStats = this.m_btnStats.GetComponent<Button>();
        Button btnBar3 = this.m_btnBar3.GetComponent<Button>();
        Button btnBar2 = this.m_btnBar2.GetComponent<Button>();
        Button btnLine = this.m_btnLine.GetComponent<Button>();
        Button btnPie = this.m_btnPie.GetComponent<Button>();
		Button btnAdvanceBase = this.m_btnAdvanceBase.GetComponent<Button>();
		Button btnFuturesDefence = this.m_btnFuturesDefence.GetComponent<Button>();
		Button btnFuturesOffence = this.m_btnFuturesOffence.GetComponent<Button>();
		Button btnPinchHitter = this.m_btnPinchHitter.GetComponent<Button>();
		Button btnSteal = this.m_btnSteal.GetComponent<Button>();
		Button btnBallType = this.m_btnBallType.GetComponent<Button>();

        Button btnStop = this.m_btnStop.GetComponent<Button>();

        btnStats.onClick.AddListener(onClickStats);
        btnBar3.onClick.AddListener(onClickBar3);
        btnBar2.onClick.AddListener(onClickBar2);
        btnLine.onClick.AddListener(onClickLine);
        btnPie.onClick.AddListener(onClickPie);
		btnFuturesDefence.onClick.AddListener(onClickFuturesDefence);
		btnFuturesOffence.onClick.AddListener(onClickFuturesOffence);
		btnPinchHitter.onClick.AddListener (onClickPinchHitter);
		btnSteal.onClick.AddListener (onClickSteal);
		btnAdvanceBase.onClick.AddListener (onClickAdvanceBase);
        btnBallType.onClick.AddListener(onClickBallType);
        btnStop.onClick.AddListener(onClickStop);

#if UNITY_EDITOR
        gameObject.SetActive(true);
#else
        gameObject.SetActive(false);
#endif
    }

    void callStatVisualization(string p_sJsonData)
    {
        Debug.Log("JSON : " + p_sJsonData);
        this.m_cStatVisualization.startStatVisualization(p_sJsonData);
    }

    void stopStatVisualization()
    {
        Debug.Log("STOP");
        this.m_cStatVisualization.stopStatVisualization();
    }

    public void onClickStats()
    {
        StartCoroutine("loadChartData", "stats.txt");
    }

    public void onClickBar3()
    {
        StartCoroutine("loadChartData", "bar3.txt");
    }

    public void onClickBar2()
    {
        StartCoroutine("loadChartData", "bar2.txt");
    }

    public void onClickLine()
    {
        StartCoroutine("loadChartData", "line.txt");
    }

    public void onClickPie()
    {
        StartCoroutine("loadChartData", "pie.txt");
    }

	public void onClickFuturesDefence() {
		StartCoroutine("loadChartData", "futuresDefence.txt");
	}

	public void onClickFuturesOffence() {
		StartCoroutine("loadChartData", "futuresOffence.txt");
	}

	public void onClickPinchHitter() {
		StartCoroutine("loadChartData", "pinchHitter.txt");
	}

	public void onClickSteal() {
		StartCoroutine("loadChartData", "steal.txt");
	}

	public void onClickAdvanceBase()
	{
		StartCoroutine("loadChartData", "advanceBase.txt");
	}

	public void onClickBallType() {
		StartCoroutine("loadChartData", "ballType.txt");
	}

    public void onClickStop()
    {
        StartCoroutine("stopChartData", "");
    }

    IEnumerator loadChartData(string p_sFileName)
    {
		string sDataFilePath = "file://C:/BinaryCruise/Source/StatsVisualization/Build/Data/chart/";
        WWW www = new WWW(sDataFilePath + p_sFileName);

        yield return www;

        try
        {
            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.Log("ERROR : " + www.error);
            }
            else if (www.size > 0)
            {
                string sJsonData = www.text;

                this.callStatVisualization(sJsonData);
            }
        }
        catch
        {
            Debug.Log("ERROR while fetching player image!");
        }
    }

    IEnumerator stopChartData(object o)
    {
        this.stopStatVisualization();

        yield return new WaitForSeconds(0.0f);
    }

}
