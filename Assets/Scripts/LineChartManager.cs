﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class LineChartManager : MonoBehaviour
{
    public GameObject m_goLineChartTitleContainer;

    private Vector3[] m_vXAxisPoint;

    private GameObject m_goGraphContainer = null;
    private GameObject m_goGridContainer = null;

    private int m_nDrawHorizontalLineCount = 10;
    private float m_fHorizontalLineStartY = 34f;
    private float m_fHorizontalLineStep = -2.5f;
    private float m_fVerticalLineStartX = -12f;
    private float m_fVerticalLineStep = 12.8f;

    private int m_nMaxXAxisCount = 5;
    private float m_fMinLineScore;

    public void initLineChart(StatsPlayInfo p_statsPlayInfo)
    {
        gameObject.SetActive(true);

        this.m_vXAxisPoint = new Vector3[this.m_nMaxXAxisCount];
        this.m_fMinLineScore = 0f;

        this.createGraphContainer();
        this.createGridContainer();

        this.drawLineChartTitle(p_statsPlayInfo.title);

        this.drawHorizontalLine(p_statsPlayInfo);
        this.drawVerticalLine((float)p_statsPlayInfo.y_axis_max, p_statsPlayInfo.data);

        StartCoroutine("drawLineChart", p_statsPlayInfo);
    }

    void createGraphContainer()
    {
        this.m_goGraphContainer = new GameObject();
        this.m_goGraphContainer.name = "GraphContainer";
        this.m_goGraphContainer.transform.SetParent(transform);
        this.m_goGraphContainer.transform.localPosition = new Vector3(0f, 0f, 0f);
        this.m_goGraphContainer.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
    }

    void createGridContainer()
    {
        this.m_goGridContainer = new GameObject();
        this.m_goGridContainer.name = "GridContainer";
        this.m_goGridContainer.transform.SetParent(transform);
        this.m_goGridContainer.transform.localPosition = new Vector3(0f, 0f, 2f);
        this.m_goGridContainer.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
    }

    void drawLineChartTitle(string p_sTitle)
    {
        Transform tLineChartTitle = this.m_goLineChartTitleContainer.transform.FindChild("LineChartTitle");
        TextMesh tLineTitleTextMesh = tLineChartTitle.GetComponent<TextMesh>();
        tLineTitleTextMesh.text = p_sTitle;

        Transform tLineChartTitleBg = this.m_goLineChartTitleContainer.transform.FindChild("LineChartTitleBg");
        Vector3 vOrgScale = tLineChartTitleBg.localScale;
        vOrgScale.x = (float)(((tLineTitleTextMesh.fontSize * 0.09) * p_sTitle.Length) * tLineChartTitle.localScale.x) + 2f;

        tLineChartTitleBg.localScale = vOrgScale;

        this.m_goLineChartTitleContainer.SetActive(true);
    }

    void drawHorizontalLine(StatsPlayInfo p_statsPlayInfo)
    {
        float fScoreDesc = (float)((p_statsPlayInfo.y_axis_max - p_statsPlayInfo.y_axis_min) / (this.m_nDrawHorizontalLineCount - 1));
        float fStartScore = (float)p_statsPlayInfo.y_axis_max;
        float fYAxis = 0f;

        for (int i = 0; i < this.m_nDrawHorizontalLineCount; i++)
        {
            fYAxis = this.m_fHorizontalLineStartY + (i * this.m_fHorizontalLineStep);

            GameObject goHorizontalLine = Instantiate(Resources.Load("Prefabs/pfLineChartGridHorizontalLine")) as GameObject;
            goHorizontalLine.transform.SetParent(this.m_goGridContainer.transform);
            goHorizontalLine.transform.localPosition = new Vector3(0f, fYAxis, 0f);
            goHorizontalLine.transform.localEulerAngles = new Vector3(0f, 0f, 0f);

            Transform scoreChild = goHorizontalLine.transform.FindChild("score");
            if (i % 2 == 0)
            {
                scoreChild.GetComponent<TextMesh>().text = fStartScore.ToString("F2");
                scoreChild.gameObject.SetActive(true);
            }
            else
            {
                scoreChild.gameObject.SetActive(false);
            }

            fStartScore -= fScoreDesc;
        }

        this.m_fMinLineScore = fStartScore;
    }

    void drawVerticalLine(float p_fYAxisMax, StatsPlayInfoItem[] p_statsPlayInfoItems)
    {
        for (int i = 0; i < this.m_nMaxXAxisCount + 1; i++)
        {
            StatsPlayInfoItem statsPlayInfoItem = p_statsPlayInfoItems[i];

            GameObject goVerticalLine = Instantiate(Resources.Load("Prefabs/pfLineChartGridVerticalLine")) as GameObject;
            goVerticalLine.transform.SetParent(this.m_goGridContainer.transform);
            goVerticalLine.transform.localPosition = new Vector3(this.m_fVerticalLineStartX + (i * this.m_fVerticalLineStep), 23f, 0f);
            goVerticalLine.transform.localEulerAngles = new Vector3(0f, 0f, 0f);

            Transform scoreChild = goVerticalLine.transform.FindChild("score");

            if (i < this.m_nMaxXAxisCount)
            {
                scoreChild.GetComponent<TextMesh>().text = statsPlayInfoItem.x_title1;
                this.calculatePoint(i, goVerticalLine.transform.localPosition, scoreChild.transform.localPosition, p_fYAxisMax, statsPlayInfoItem);
            }
            else
            {
                scoreChild.gameObject.SetActive(false);
            }
        }
    }

    void calculatePoint(int p_nSeq, Vector3 p_vLinePosition, Vector3 p_vScoreChildPosition, float p_fYAxisMax, StatsPlayInfoItem p_statsPlayInfoItem)
    {
        float fMinYAxisPoint = this.m_fHorizontalLineStartY - Mathf.Abs((this.m_nDrawHorizontalLineCount - 1) * this.m_fHorizontalLineStep);

        float fDiff = p_fYAxisMax - this.m_fMinLineScore;
        float fActualVal = (float)(p_statsPlayInfoItem.y_value - this.m_fMinLineScore);
        float fYAxisPoint = (((this.m_fHorizontalLineStartY - fMinYAxisPoint) * fActualVal) / fDiff) + fMinYAxisPoint;

        float fXAxisPoint = p_vLinePosition.x + p_vScoreChildPosition.x;

        this.m_vXAxisPoint[p_nSeq] = new Vector3(fXAxisPoint, fYAxisPoint, p_vScoreChildPosition.z);
    }

    IEnumerator drawLineChart(StatsPlayInfo p_statsPlayInfo)
    {
        yield return new WaitForSeconds(1.0f);

        this.transform.GetComponent<AudioSource>().PlayDelayed(0.0f);

        //AudioSource.PlayClipAtPoint(Resources.Load("Sounds/sound_line") as AudioClip, transform.position, 100);

        for (int i = 0; i < this.m_nMaxXAxisCount; i++)
        {
            StatsPlayInfoItem statsPlayInfoItem = p_statsPlayInfo.data[i];
            Vector3 startPosition = this.m_vXAxisPoint[i];

            this.drawLineScore(startPosition, statsPlayInfoItem.x_title2);

            int targetSeq = i + 1;
            if (targetSeq < this.m_vXAxisPoint.Length)
            {
                Vector3 targetPosition = this.m_vXAxisPoint[targetSeq];
                Vector3 virtualPosition = new Vector3(targetPosition.x, startPosition.y, startPosition.z);

                float angle = ContAngle(virtualPosition - startPosition, targetPosition - startPosition);//Vector3.Angle(targetPosition, startPosition); //
                float dist = Vector3.Distance(startPosition, targetPosition) + 0.6f;

                if (targetPosition.y < startPosition.y)
                {
                    angle = angle * -1;
                }

                float positionX = startPosition.x + ((targetPosition.x - startPosition.x) / 2);
                float positionY = startPosition.y + ((targetPosition.y - startPosition.y) / 2);

                GameObject goLine3D = Instantiate(Resources.Load("Prefabs/pfLine3D")) as GameObject;
                goLine3D.transform.SetParent(this.m_goGraphContainer.transform);
                goLine3D.transform.localEulerAngles = new Vector3(0f, 0f, angle);
                goLine3D.transform.localScale = new Vector3(1f, 1f, 1f);
                goLine3D.transform.localPosition = startPosition;

                TweenScale twScale = UITweener.Begin<TweenScale>(goLine3D, 0.3f/*0.5f*/);
                twScale.from = new Vector3(1f, 1f, 1f);
                twScale.to = new Vector3(dist, 1f, 1f);
                twScale.ResetToBeginning();
                twScale.PlayForward();

                TweenPosition twPosition = TweenPosition.Begin<TweenPosition>(goLine3D, 0.3f/*0.5f*/);
                twPosition.from = startPosition;
                twPosition.to = new Vector3(positionX, positionY, startPosition.z);
                twPosition.ResetToBeginning();
                twPosition.PlayForward();

                yield return new WaitForSeconds(0.3f/*0.5f*/);
            }
        }
    }

    void drawLineScore(Vector3 _vPosition, string _sScore)
    {
        GameObject goLineScore = Instantiate(Resources.Load("Prefabs/pfLineScore")) as GameObject;
        goLineScore.SetActive(false);
        goLineScore.transform.SetParent(this.m_goGraphContainer.transform);
        goLineScore.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
        goLineScore.transform.localPosition = new Vector3(_vPosition.x, _vPosition.y + 5, _vPosition.z);

        goLineScore.GetComponent<TextMesh>().text = _sScore;

        goLineScore.AddComponent<AutoTyping>();
        goLineScore.GetComponent<AutoTyping>().m_fStartDelayTime = 0.0f;
        goLineScore.GetComponent<AutoTyping>().typingTime = 0.1f;

        goLineScore.SetActive(true);
    }

    public float ContAngle(Vector3 fwd, Vector3 targetDir)
    {
        float angle = Vector3.Angle(fwd, targetDir);

        if (AngleDir(fwd, targetDir, Vector3.up) == -1)
        {
            angle = 360.0f - angle;
            if (angle > 359.9999f)
                angle -= 360.0f;
            return angle;
        }
        else
            return angle;
    }

    public int AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, up);

        if (dir > 0.0)
            return 1;
        else if (dir < 0.0)
            return -1;
        else
            return 0;
    }

    public void removeChart()
    {
        if (this.m_goGraphContainer != null)
        {
            Destroy(this.m_goGraphContainer);
        }

        if (this.m_goGridContainer != null)
        {
            Destroy(this.m_goGridContainer);
        }

        this.m_goLineChartTitleContainer.SetActive(false);
    }
}
