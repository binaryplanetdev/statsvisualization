﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StatsPannel : MonoBehaviour {
    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void setParent(Transform _tParent)
    {
		transform.SetParent(_tParent);
    }

    public void showStatsPannel(StatsPlayInfoItem p_statsPlayInfoItem, float p_fPositionY)
    {
		transform.localPosition = new Vector3 (-960f, p_fPositionY, 0f);
		transform.localScale = new Vector3(1f, 1f, 1f);	

        Transform tfStatsTitle = transform.Find("StatsTitle");
        Transform tfStatsValue1 = transform.Find("Stats1");
        Transform tfStatsValue2 = transform.Find("Stats2");
        Transform tfStatsValue3 = transform.Find("Stats3");
        Transform tfStatsValue4 = transform.Find("Stats4");
        Transform tfStatsValue5 = transform.Find("Stats5");
        Transform tfStatsSeasonScore = transform.Find("SeasonScore");

		Sprite sprLost = (Sprite)Resources.Load("Images/Sprites/statsbtnlost", typeof(Sprite));
		Sprite sprWin = (Sprite)Resources.Load("Images/Sprites/statsbtnwin", typeof(Sprite));

		if (tfStatsTitle != null)
        {
            Text txtTitleMesh = tfStatsTitle.transform.Find("Title").GetComponentInChildren<Text>();
			txtTitleMesh.text = p_statsPlayInfoItem.x_title1;
            Text txtMesh = tfStatsTitle.transform.Find("Text").GetComponentInChildren<Text>();
			txtMesh.text = p_statsPlayInfoItem.x_title2;

			StartCoroutine ("playStatsTitleMove", tfStatsTitle.gameObject);
        }

		if (tfStatsValue1 != null)
        {
			if (p_statsPlayInfoItem.y_result1.ToUpper().Equals("L")) tfStatsValue1.GetComponent<Image>().sprite = sprLost;
            else tfStatsValue1.GetComponent<Image>().sprite = sprWin;

            /// 팀이미지 변경
            /// 
            Image imageTeam = tfStatsValue1.transform.Find("Team").GetComponentInChildren<Image>();
            StartCoroutine("loadTeamImage", new object[] { p_statsPlayInfoItem.y_value1_path, imageTeam });

            Text txtMesh = tfStatsValue1.transform.Find("Value").GetComponentInChildren<Text>();
            txtMesh.text = p_statsPlayInfoItem.y_value1;

            StartCoroutine("playChangeAlpha", tfStatsValue1.gameObject);
        }

        if (tfStatsValue2 != null)
        {
            if (p_statsPlayInfoItem.y_result2.ToUpper().Equals("L")) tfStatsValue2.GetComponent<Image>().sprite = sprLost;
            else tfStatsValue2.GetComponent<Image>().sprite = sprWin;

            /// 팀이미지 변경
            /// 
            Image imageTeam = tfStatsValue2.transform.Find("Team").GetComponentInChildren<Image>();
            StartCoroutine("loadTeamImage", new object[] { p_statsPlayInfoItem.y_value2_path, imageTeam });

            Text txtMesh = tfStatsValue2.transform.Find("Value").GetComponentInChildren<Text>();
            txtMesh.text = p_statsPlayInfoItem.y_value2;

            StartCoroutine("playChangeAlpha", tfStatsValue2.gameObject);
        }

        if (tfStatsValue3 != null)
        {
            if (p_statsPlayInfoItem.y_result3.ToUpper().Equals("L")) tfStatsValue3.GetComponent<Image>().sprite = sprLost;
            else tfStatsValue3.GetComponent<Image>().sprite = sprWin;

            /// 팀이미지 변경
            /// 
            Image imageTeam = tfStatsValue3.transform.Find("Team").GetComponentInChildren<Image>();
            StartCoroutine("loadTeamImage", new object[] { p_statsPlayInfoItem.y_value3_path, imageTeam });

            Text txtMesh = tfStatsValue3.transform.Find("Value").GetComponentInChildren<Text>();
            txtMesh.text = p_statsPlayInfoItem.y_value3;

            StartCoroutine("playChangeAlpha", tfStatsValue3.gameObject);
        }

        if (tfStatsValue4 != null)
        {
            if (p_statsPlayInfoItem.y_result4.ToUpper().Equals("L")) tfStatsValue4.GetComponent<Image>().sprite = sprLost;
            else tfStatsValue4.GetComponent<Image>().sprite = sprWin;

            /// 팀이미지 변경
            /// 
            Image imageTeam = tfStatsValue4.transform.Find("Team").GetComponentInChildren<Image>();
            StartCoroutine("loadTeamImage", new object[] { p_statsPlayInfoItem.y_value4_path, imageTeam });

            Text txtMesh = tfStatsValue4.transform.Find("Value").GetComponentInChildren<Text>();
            txtMesh.text = p_statsPlayInfoItem.y_value4;

            StartCoroutine("playChangeAlpha", tfStatsValue4.gameObject);
        }

        if (tfStatsValue5 != null)
        {
            if (p_statsPlayInfoItem.y_result5.ToUpper().Equals("L")) tfStatsValue5.GetComponent<Image>().sprite = sprLost;
            else tfStatsValue5.GetComponent<Image>().sprite = sprWin;
            /// 팀이미지 변경
            /// 
            Image imageTeam = tfStatsValue5.transform.Find("Team").GetComponentInChildren<Image>();
            StartCoroutine("loadTeamImage", new object[] { p_statsPlayInfoItem.y_value5_path, imageTeam });

            Text txtMesh = tfStatsValue5.transform.Find("Value").GetComponentInChildren<Text>();
            txtMesh.text = p_statsPlayInfoItem.y_value5;

            StartCoroutine("playChangeAlpha", tfStatsValue5.gameObject);
        }

        if (tfStatsSeasonScore != null)
        {
            Text txtMesh = tfStatsSeasonScore.transform.GetComponentInChildren<Text>();
            txtMesh.text = p_statsPlayInfoItem.y_value6;

            StartCoroutine("playChangeAlpha", tfStatsSeasonScore.gameObject);
        }
    }

	IEnumerator playStatsTitleMove(GameObject p_goStatsTitle)
	{
		Vector3 originPos = p_goStatsTitle.transform.localPosition;

		TweenPosition twPosition = TweenPosition.Begin<TweenPosition>(p_goStatsTitle, 1f);
		twPosition.from = originPos;
		twPosition.to = new Vector3(originPos.x + 400f, originPos.y, originPos.z);
		twPosition.ResetToBeginning();
		twPosition.PlayForward();

		yield return new WaitForSeconds(0.0f);
	}

	IEnumerator loadTeamImage(object[] p_oParams)
	{
	    string p_sFileName = (string)p_oParams[0];
	    Image p_iTeamImageObject = (Image)p_oParams[1];

	    WWW www = new WWW(StatVisualization.TEAM_IMAGE_PATH + p_sFileName);
        
        yield return www;

	    try
	    {
	        if (!string.IsNullOrEmpty(www.error))
	        {
	            Debug.Log("ERROR : " + www.error);
                p_iTeamImageObject.gameObject.SetActive(false);

            }
            else if (www.size > 0)
	        {
	            Texture2D tTeamTexture = www.texture;
	            Rect rTeamTextureRect = new Rect(0f, 0f, tTeamTexture.width, tTeamTexture.height);

	            p_iTeamImageObject.sprite = Sprite.Create(tTeamTexture, rTeamTextureRect, new Vector2(0.5f, 0.5f));
	            p_iTeamImageObject.rectTransform.localScale = new Vector3(1f, 1f, 1f);
                
	        }
	    }
	    catch
	    {
	        Debug.Log("ERROR while fetching player image!");
            try
            {
                p_iTeamImageObject.gameObject.SetActive(false);
            }
            catch { }
        }
	}

	int indexAlphaAnimation = 0;
	IEnumerator playChangeAlpha(GameObject go)
	{
		CanvasRenderer[] arrCr = go.GetComponentsInChildren<CanvasRenderer>();

		if (arrCr != null)
		{
			foreach (CanvasRenderer cr in arrCr)
				cr.SetAlpha(0.0f);

			yield return new WaitForSeconds(0.0f + ((float)(indexAlphaAnimation++) * 0.01f/*0.02f*/));

			if (arrCr != null)
			{

				float start = Time.time;
				while (arrCr[0].GetAlpha() != 1.0f)
				{
					float elapsed = Time.time - start;
					float normalisedTime = Mathf.Clamp((elapsed / 1.0f) * Time.deltaTime, 0, 1);
					foreach (CanvasRenderer cr in arrCr)
						cr.SetAlpha(Mathf.Lerp(cr.GetAlpha(), 1.0f, normalisedTime));

					yield return 0;
				}
			}
		}

		yield return true;
	}
}
