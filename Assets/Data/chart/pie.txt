{
	"pl_no": "29",
	"pl_nm": "김광현",
	"pl_img_path": "29.png",
	"pl_pos": "Starting Pitcher",
	"ctype": "pie",
	"title": "경기투구분석",
	"y_axis_min": 0.22,
	"y_axis_max": 0.4,
	"x_axis_cnt": 5,
	"data": [{
		"y_value": 10.0,
		"x_title1": "포심",
		"x_title2": "10",
		"type": "",
		"y_value1": "",
		"y_value1_path": "",
		"y_result1": "",
		"y_value2": "",
		"y_value2_path": "",
		"y_result2": "",
		"y_value3": "",
		"y_value3_path": "",
		"y_result3": "",
		"y_value4": "",
		"y_value4_path": "",
		"y_result4": "",
		"y_value5": "",
		"y_value5_path": "",
		"y_result5": "",
		"y_value6": ""
	},
	{
		"y_value": 23.0,
		"x_title1": "커브",
		"x_title2": "23",
		"type": "",
		"y_value1": "",
		"y_value1_path": "",
		"y_result1": "",
		"y_value2": "",
		"y_value2_path": "",
		"y_result2": "",
		"y_value3": "",
		"y_value3_path": "",
		"y_result3": "",
		"y_value4": "",
		"y_value4_path": "",
		"y_result4": "",
		"y_value5": "",
		"y_value5_path": "",
		"y_result5": "",
		"y_value6": ""
	},
	{
		"y_value": 23.0,
		"x_title1": "포크볼",
		"x_title2": "23",
		"type": "",
		"y_value1": "",
		"y_value1_path": "",
		"y_result1": "",
		"y_value2": "",
		"y_value2_path": "",
		"y_result2": "",
		"y_value3": "",
		"y_value3_path": "",
		"y_result3": "",
		"y_value4": "",
		"y_value4_path": "",
		"y_result4": "",
		"y_value5": "",
		"y_value5_path": "",
		"y_result5": "",
		"y_value6": ""
	},
	{
		"y_value": 12.0,
		"x_title1": "싱커",
		"x_title2": "12",
		"type": "",
		"y_value1": "",
		"y_value1_path": "",
		"y_result1": "",
		"y_value2": "",
		"y_value2_path": "",
		"y_result2": "",
		"y_value3": "",
		"y_value3_path": "",
		"y_result3": "",
		"y_value4": "",
		"y_value4_path": "",
		"y_result4": "",
		"y_value5": "",
		"y_value5_path": "",
		"y_result5": "",
		"y_value6": ""
	},
	{
		"y_value": 2.0,
		"x_title1": "너클",
		"x_title2": "2",
		"type": "",
		"y_value1": "",
		"y_value1_path": "",
		"y_result1": "",
		"y_value2": "",
		"y_value2_path": "",
		"y_result2": "",
		"y_value3": "",
		"y_value3_path": "",
		"y_result3": "",
		"y_value4": "",
		"y_value4_path": "",
		"y_result4": "",
		"y_value5": "",
		"y_value5_path": "",
		"y_result5": "",
		"y_value6": ""
	}]
}