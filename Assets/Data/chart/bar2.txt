﻿{
	"pl_no": "14",
	"pl_nm": "최정",
	"pl_img_path": "14.png",
	"pl_pos": "Third Base Man",
	"ctype": "bar",
	"title": "득점권 타율",
	"y_axis_min": 0.06,
	"y_axis_max": 0.46,
	"x_axis_cnt": 2,
	"data": [{
		"y_value": 0.123,
		"x_title1": "득점권타율",
		"x_title2": "0.123",
		"type": "",
		"y_value1": "",
		"y_value1_path": "",
		"y_result1": "",
		"y_value2": "",
		"y_value2_path": "",
		"y_result2": "",
		"y_value3": "",
		"y_value3_path": "",
		"y_result3": "",
		"y_value4": "",
		"y_value4_path": "",
		"y_result4": "",
		"y_value5": "",
		"y_value5_path": "",
		"y_result5": "",
		"y_value6": ""
	},
	{
		"y_value": 0.2,
		"x_title1": "상대투수타율",
		"x_title2": "0.2",
		"type": "",
		"y_value1": "",
		"y_value1_path": "",
		"y_result1": "",
		"y_value2": "",
		"y_value2_path": "",
		"y_result2": "",
		"y_value3": "",
		"y_value3_path": "",
		"y_result3": "",
		"y_value4": "",
		"y_value4_path": "",
		"y_result4": "",
		"y_value5": "",
		"y_value5_path": "",
		"y_result5": "",
		"y_value6": ""
	}]
}