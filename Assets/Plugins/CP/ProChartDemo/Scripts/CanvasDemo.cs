﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using CP.ProChart;

///<summary>
/// Demo for canvas based bar and line chart using 2D data set
///</summary>
public class CanvasDemo : MonoBehaviour 
{
	//bar chart datas
	public BarChart barChart;
	public Slider spacing;
	public Slider barThickness;
	public InputField distanceInput;
	public InputField marginInput;

	//line chart datas
	public LineChart lineChart;
	public Slider thickness;
	public Slider pointSize;
	public InputField thicknessInput;
	public InputField pointSizeInput;
	
	//data value change items
	public GameObject dataPanel;
	public Text infoData;
	public Slider data;
	public InputField dataInput;
	public Text info;

	//tooltip items
	public RectTransform tooltip;
	public Text tooltipText;

	//2D Data set
	private ChartData2D dataSet;

	//selection of data
	private int row = -1;
	private int column = -1;
	private int overRow = -1;
	private int overColumn = -1;

	///<summary>
	/// Manage selection of data to be able to change it
	///</summary>
	public void OnSelectDelegate(int row, int column)
	{
		this.row = row;
		this.column = column;
		infoData.gameObject.SetActive(false);
		dataPanel.SetActive(true);
		data.value = dataSet[row, column];
		info.text = string.Format("Data[{0},{1}]", row, column);
		dataInput.text = dataSet[row, column].ToString();
	}

	///<summary>
	/// Manage over state of chart
	///</summary>
	public void OnOverDelegate(int row, int column)
	{
		overRow = row;
		overColumn = column;
	}

	///<summary>
	/// Initialize data set and charts
	///</summary>
	void OnEnable() 
	{
		spacing.value = barChart.Spacing;
		barThickness.value = barChart.Thickness;

		thickness.value = lineChart.Thickness;
		pointSize.value = lineChart.PointSize;

		dataSet = new ChartData2D();
		dataSet[0, 0] = 50;
		dataSet[0, 1] = 30;
		dataSet[0, 2] = 70;
		dataSet[0, 3] = 10;
		dataSet[0, 4] = 90;
		dataSet[1, 0] = 40;
		dataSet[1, 1] = 25;
		dataSet[1, 2] = 53;
		dataSet[1, 3] = 12;
		dataSet[1, 4] = 37;
		dataSet[2, 0] = 68;
		dataSet[2, 1] = 91;
		dataSet[2, 2] = 30;
		dataSet[2, 3] = 44;
		dataSet[2, 4] = 63;

		barChart.SetValues(ref dataSet);
		lineChart.SetValues(ref dataSet);

		barChart.onSelectDelegate += OnSelectDelegate;
		barChart.onOverDelegate += OnOverDelegate;
		lineChart.onSelectDelegate += OnSelectDelegate;
		lineChart.onOverDelegate += OnOverDelegate;
	
		distanceInput.text = barChart.Spacing.ToString("0.00");
		marginInput.text = barThickness.value.ToString("0.00");
		thicknessInput.text = thickness.value.ToString("0.00");
		pointSizeInput.text = pointSize.value.ToString("0.00");
	}

	///<summary>
	/// Remove hanlders when object is disabled
	///</summary>
	void OnDisable()
	{
		barChart.onSelectDelegate -= OnSelectDelegate;
		barChart.onOverDelegate -= OnOverDelegate;
		lineChart.onSelectDelegate -= OnSelectDelegate;
		lineChart.onOverDelegate -= OnOverDelegate;
	}

	///<summary>
	/// manage tooltip
	///</summary>
	void Update ()
	{
		tooltip.gameObject.SetActive(overRow != -1);
		if (overRow != -1)
		{
			tooltip.anchoredPosition = (Vector2)Input.mousePosition + tooltip.sizeDelta * tooltip.localScale.x / 2;
			tooltipText.text = string.Format("Data[{0},{1}]\nValue: {2:F2}", overRow, overColumn, dataSet[overRow, overColumn]);
		}
	}

	///<summary>
	/// Change the type of chart with click on buttons
	///</summary>
	public void OnClick(string button)
	{
		if (button == "curve")
		{
			lineChart.Chart = LineChart.ChartType.CURVE;
		}
		else if (button == "line")
		{
			lineChart.Chart = LineChart.ChartType.LINE;
		}
		else if (button == "none")
		{
			lineChart.Point = LineChart.PointType.NONE;
		}
		else if (button == "rectangle")
		{
			lineChart.Point = LineChart.PointType.RECTANGLE;
		}
		else if (button == "circle")
		{
			lineChart.Point = LineChart.PointType.CIRCLE;
		}
		else if (button == "triangle")
		{
			lineChart.Point = LineChart.PointType.TRIANGLE;
		}
	}
	
	///<summary>
	/// Update values when slider moved
	///</summary>
	public void OnValueChanged(string slider)
	{
		if (slider == "distance")
		{
			barChart.Spacing = spacing.value;
			distanceInput.text = barChart.Spacing.ToString("0.00");
		}
		else if (slider == "distanceInput")
		{
			barChart.Spacing = float.Parse(distanceInput.text);
			spacing.value = barChart.Spacing;
			distanceInput.text = spacing.value.ToString("0.00");
		}		
		else if (slider == "margin")
		{
			barChart.Thickness = barThickness.value;
			marginInput.text = barThickness.value.ToString("0.00");
		}
		else if (slider == "marginInput")
		{
			barChart.Thickness = float.Parse(marginInput.text);
			barThickness.value = barChart.Thickness;
			marginInput.text = barThickness.value.ToString("0.00");
		}		
		else if (slider == "thickness")
		{
			lineChart.Thickness = thickness.value;
			thicknessInput.text = thickness.value.ToString("0.00");
		}
		else if (slider == "thicknessInput")
		{
			lineChart.Thickness = float.Parse(thicknessInput.text);
			thickness.value = lineChart.Thickness;
			thicknessInput.text = thickness.value.ToString("0.00");
		}
		else if (slider == "pointSize")
		{
			lineChart.PointSize = pointSize.value;
			pointSizeInput.text = pointSize.value.ToString("0.00");
		}
		else if (slider == "pointSizeInput")
		{
			lineChart.PointSize = float.Parse(pointSizeInput.text);
			pointSize.value = lineChart.PointSize;
			pointSizeInput.text = pointSize.value.ToString("0.00");
		}
		else if (slider == "data")
		{
			dataSet[row, column] = data.value;
			info.text = string.Format("Data[{0},{1}]", row, column);
			dataInput.text = dataSet[row, column].ToString("0.00");
		}
		else if (slider == "dataInput")
		{
			dataSet[row, column] = Mathf.Clamp(float.Parse(dataInput.text), -100, 100);
			data.value = dataSet[row, column];
			dataInput.text = dataSet[row, column].ToString();
		}
	}
} //class
