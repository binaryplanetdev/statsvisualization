﻿using UnityEngine;
using System.Collections;

///<summary>
/// Menu class to transfer jumping function to load a scene
///</summary>
public class Menu : MonoBehaviour
{
	public void GoCanvasDemo()
	{
		Application.LoadLevel("CanvasDemo");
	}

	public void GoCanvasDemoBarAndLine()
	{
		Application.LoadLevel("CanvasDemoBarAndLine");
	}

	public void GoCanvasDemoPie()
	{
		Application.LoadLevel("CanvasDemoPie");
	}

	public void GoCountdownDemo()
	{
		Application.LoadLevel("CountdownDemo");
	}

	public void GoMeshDemo()
	{
		Application.LoadLevel("MeshDemo");
	}

	public void GoMeshDemo3D()
	{
		Application.LoadLevel("MeshDemo3D");
	}

	public void GoMenu()
	{
		Application.LoadLevel("Menu");
	}
}
