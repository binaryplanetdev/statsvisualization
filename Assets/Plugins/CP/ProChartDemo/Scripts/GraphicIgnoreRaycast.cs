﻿using UnityEngine;
 
///<summary>
/// Helper for tooltip to avoid capture mouse input
/// Attached to tooltip object, hence it won't capture mouse from charts
///</summary>
public class GraphicIgnoreRaycast : MonoBehaviour, ICanvasRaycastFilter
{
	public bool IsRaycastLocationValid(Vector2 screenPoint, Camera eventCamera)
	{
		return false;
	}
}